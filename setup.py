from distutils.core import setup

install_requires = [
    'boto3>=1.20.24'
    'botocore>=1.23.24'
    'jmespath>=0.10.0'
    'numpy>=1.21.4'
    'pandas>=1.3.5'
    'python-dateutil>=2.8.2'
    'python-dotenv>=0.19.2'
    'pytz>=2021.3'
    's3transfer>=0.5.0'
    'six>=1.16.0'
    'urllib3>=1.26.7'
]

setup(
    name='gsheets_to_s3',
    version='1.0',
    packages=['gsheets_to_s3'],
    long_description='Writes google sheets to S3',
    install_requires=install_requires,
)
