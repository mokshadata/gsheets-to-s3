# Import libraries
import os
import gspread
from google.oauth2.service_account import Credentials


# Define functions to pull Google Sheet data
def auth_google_acct(credentials):
    """
    Returns authenticated Client object for Google Shoots API service account

    :param credentials: json, service account credentials
    """
    scopes = [
        'https://www.googleapis.com/auth/spreadsheets',
        'https://www.googleapis.com/auth/drive'
    ]
    credentials = Credentials.from_service_account_info(
        info=credentials,
        scopes=scopes
    )
    gc = gspread.authorize(credentials)
    return gc


def pull_google_sheet(gc, spreadsheet, tab):
    """
    Returns Google Sheet data from specified worksheet and tab as a list of lists

    :param gc: Client, authenticated Client object
    :param spreadsheet: str, name of Google Sheet to access
    :param tab: str, name of tab to return data from
    """
    sh = gc.open(spreadsheet)
    ws = sh.worksheet(tab).get_all_values()
    return ws


def get_worksheet(gc, spreadsheet, tab):
    """
    Returns Google Sheet data from specified worksheet and tab as a list of lists

    :param gc: Client, authenticated Client object
    :param spreadsheet: str, name of Google Sheet to access
    :param tab: str, name of tab to return data from
    """
    sh = gc.open(spreadsheet)
    ws = sh.worksheet(tab)
    return ws


def list_to_df(pd, list_data, headers, row_start=1):
    """
    Returns pandas DataFrame object from list of lists

    :param list_data: list, list of lists of the Google Sheets data
    :param headers: list, list of all header names for DataFrame
    :param row_start: int, index to start with when converting to DataFrame
    """
    return pd.DataFrame(list_data[row_start:], columns=headers)


def write_csv(df, path, filename):
    """
    Returns method to write pandas DataFrame to csv

    :param df: DataFrame, pandas DataFrame object to write to csv
    :param path: str, path and name to write csv to
    """
    return df.to_csv(os.path.join(path, filename), index=False)


def write_csv_body_to_s3(s3_client, csv_data, s3_bucket, s3_path):
    return s3_client.put_object(Body=csv_data, Bucket=s3_bucket, Key=s3_path)
