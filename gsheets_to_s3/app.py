import boto3
import pandas as pd
from dotenv import load_dotenv

import os
import logging
import sys
import argparse
import json
from utils import (
    auth_google_acct, write_csv_body_to_s3,
    get_worksheet, write_csv
)

parser = argparse.ArgumentParser()
parser.add_argument("--debug", help="show debug logs", action="store_true")
args = parser.parse_args()
log_level = logging.INFO
if args.debug:
    log_level = logging.DEBUG
logging.basicConfig(stream=sys.stdout, level=log_level, format='%(message)s')
logger = logging.getLogger(__name__)


def get_secret(sm_client, vault_key):
    secret_value = sm_client.get_secret_value(SecretId=vault_key)
    return json.loads(secret_value['SecretString'])


def gsheets_to_s3():
    load_dotenv()
    session = boto3.session.Session()
    sm_client = session.client(
        service_name='secretsmanager',
        region_name='us-east-1'
    )
    secret_key = os.getenv('VAULT_KEY', '')
    google_secret = get_secret(sm_client, secret_key)
    gc = auth_google_acct(google_secret)

    block_details = json.loads(os.getenv('BLOCK_DETAILS', ''))
    spreadsheet_name = block_details['spreadsheet_name']
    tab = block_details['tab']

    logger.info(f'Getting spreadsheet {spreadsheet_name}')
    logger.info(f'Tab {tab}')

    sheet = get_worksheet(gc, spreadsheet_name, tab)
    data = sheet.get_all_values()
    headers = data.pop(0)
    logger.debug(headers)
    logger.info('Loading spreadsheet into dataframe')
    clean_data = pd.DataFrame(data, columns=headers)
    logger.debug(clean_data)

    output_name = os.getenv('BLOCK_ID', '').lower().replace(' ', '_')
    if not os.getenv('OCEAN_RUN', ''):
        logger.info(f'Writing local file: {output_name}.csv')
        write_csv(clean_data, '.', f'{output_name}.csv')
        return f'Successfully wrote to file {output_name}.csv'

    s3_client = session.client(
            service_name='s3',
            region_name='us-east-1',
    )
    logger.info('Converting to csv')
    output_csv = clean_data.to_csv(index=False)
    s3_bucket = 'hfb-ocean-data'
    dataset = os.getenv('DATASET_ID')
    s3_path = f'data/live/{dataset}/{output_name}.csv'
    logger.info(f'Bucket: {s3_bucket}')
    logger.info(f'Path: {s3_path}')
    logger.info('Writing to S3')
    write_csv_body_to_s3(s3_client, output_csv, s3_bucket, s3_path)

    return 'Success'


if __name__ == '__main__':
    gsheets_to_s3()
